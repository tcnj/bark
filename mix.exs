defmodule Bark.MixProject do
  use Mix.Project

  def project do
    [
      app: :bark,
      version: "0.1.1",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      source_url: "https://gitlab.com/tcnj/bark",
      description: description(),
      package: package(),
      deps: deps(),
      docs: docs()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  def description do
    "Bark is a simple barcode generation library with the ability to seperately generate and render barcodes."
  end

  defp package do
    [
      licenses: ["MIT"],
      links: %{"GitLab" => "https://gitlab.com/tcnj/bark"}
    ]
  end

  defp deps do
    [
      {:ex_doc, "~> 0.14", only: :dev, runtime: false}
    ]
  end

  defp docs do
    [
      extras: ["README.md"],
      main: "readme"
    ]
  end
end
