defmodule Bark.Printer.Text do
  alias Bark.Code

  @moduledoc """
  ```text
  Copyright © 2019 Daniel Wensley

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ```

  This module implements an invertable text-based barcode printer.
  """

  @normal %{
    "" => "",
    "0" => " ",
    "1" => "▌",
    "10" => "▌",
    "11" => "█",
    "01" => "▐",
    "00" => " "
  }

  @inverted %{
    "" => "",
    "0" => "█",
    "1" => "▐",
    "10" => "▐",
    "11" => " ",
    "01" => "▌",
    "00" => "█"
  }

  def print(%Code{serialized: bars}, options \\ []) do
    table = if :invert in options do
      @inverted
    else
      @normal
    end

    base_text = print_with_table(bars, table)

    multiline = if Keyword.has_key?(options, :lines) do
      String.duplicate(<<base_text::binary, "\n">>, options[:lines])
    else
      <<base_text::binary, "\n">>
    end

    if :border in options do
      border = String.pad_leading("", String.length(base_text), table["0"])

      <<border::binary, "\n", multiline::binary, border::binary>>
    else
      String.slice(multiline, 0, String.length(multiline) - 1)
    end
  end

  defp print_with_table(bars, table, acc \\ "")
  defp print_with_table([], _table, acc), do: acc
  defp print_with_table([0], table, acc), do: <<acc::binary, table["0"]::binary>>
  defp print_with_table([1], table, acc), do: <<acc::binary, table["1"]::binary>>
  defp print_with_table([0 | [0 | tail]], table, acc), do: print_with_table(tail, table, <<acc::binary, table["00"]::binary>>)
  defp print_with_table([0 | [1 | tail]], table, acc), do: print_with_table(tail, table, <<acc::binary, table["01"]::binary>>)
  defp print_with_table([1 | [0 | tail]], table, acc), do: print_with_table(tail, table, <<acc::binary, table["10"]::binary>>)
  defp print_with_table([1 | [1 | tail]], table, acc), do: print_with_table(tail, table, <<acc::binary, table["11"]::binary>>)
end
