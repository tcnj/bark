defmodule Bark.Printer.SVG do
  alias Bark.Code

  @moduledoc """
  ```text
  Copyright © 2019 Daniel Wensley

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ```

  This module implements an SVG barcode printer.
  """

  def print(%Code{serialized: bars}, options \\ []) do
    module_width = options[:module_width] || 10
    height = options[:height] || 50
    fill_color = if :invert in options, do: "white", else: "black"

    {blocks, total_modules} = bars
      |> Enum.chunk_by(& &1)
      |> Enum.map(fn b -> {hd(b) == 1, length(b)} end)
      |> Enum.reduce({[], 0}, fn {fill, size}, {blocks, offset} ->
        if fill do
          {[{offset, size} | blocks], offset + size}
        else
          {blocks, offset + size}
        end
      end)

    total_width = total_modules * module_width

    fill_line = if :invert in options do
      ~s[<rect x="0" y="0" width="#{total_width}" height="#{height}" fill="black" />]
    else
      ""
    end

    header = ~s[<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="#{total_width}px" height="#{height}px">] <> fill_line
    footer = ~s[</svg>]

    middle = Enum.reduce(blocks, "", fn {offset, width}, acc ->
      ~s[<rect x="#{offset * module_width}" y="0" width="#{width * module_width}" height="#{height}" fill="#{fill_color}" />] <> acc
    end)

    header <> middle <> footer
  end
end
