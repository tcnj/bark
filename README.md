# Bark

Bark is a simple barcode generation library with the ability to seperately generate and render barcodes.

The following symbologies are supported:

* Code 128

The following rendering types are supported:

* Text
* SVG

These are all accessible directly under the `Bark` module or via their individual modules under `Bark.Code` or `Bark.Printer`.

## Installation

The package can be installed by adding `bark` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:bark, "~> 0.1.0"}
  ]
end
```

Documentation can be found at [https://hexdocs.pm/bark](https://hexdocs.pm/bark).